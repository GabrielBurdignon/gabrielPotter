//
//  ViewController.swift
//  gabrielPotter
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Personagem: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var atorLista : [Personagem]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return atorLista?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelula
        let ator = atorLista![indexPath.row]
        
        cell.nome.text = ator.name
        cell.ator.text = ator.actor
        cell.imagem.kf.setImage(with: URL(string: ator.image))
        
        return cell
    }
    
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        get()
        TableView.dataSource = self
    }

    private func get() {
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Personagem].self) {
            response in
            if let lista_personagem = response.value{
                self.atorLista = lista_personagem
            }
            self.TableView.reloadData()
        }
    }

}

